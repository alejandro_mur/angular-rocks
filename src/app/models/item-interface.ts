export class ItemInterface {
  key: string;
  id?: number;
  name: string;
  metaDescription: string;
  image?: string;
  genres?: string[];
  info?: string;
}
