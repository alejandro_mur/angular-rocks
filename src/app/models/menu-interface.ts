export interface MenuInterface {
  text: string;
  link: string;
}
