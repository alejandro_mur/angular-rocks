import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MainComponent } from "./components/shared/main/main.component";
import { ListComponent } from "./components/list/list.component";
import { DetailsComponent } from "./components/details/details.component";
import { AddComponent } from "./components/add/add.component";
import { SearchComponent } from "./components/search/search.component";

const routes: Routes = [
  {
    path: "",
    component: MainComponent,
    children: [
      {
        path: "listado",
        component: ListComponent,
      },
      {
        path: "buscar",
        component: SearchComponent,
      },
      {
        path: "nuevo",
        component: AddComponent,
      },
      {
        path: "detalle/:id",
        component: DetailsComponent,
      },
      { path: "", redirectTo: "listado", pathMatch: "full" },
    ],
  },
  { path: "", redirectTo: "/listado", pathMatch: "full" },
  { path: "**", redirectTo: "/listado" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
