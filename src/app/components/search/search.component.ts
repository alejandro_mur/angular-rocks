import {
  Component,
  ElementRef,
  ViewChild,
  AfterViewInit,
  Output,
  EventEmitter,
  HostBinding,
  OnInit,
} from "@angular/core";
import { FormControl, FormGroup, FormBuilder } from "@angular/forms";
import { fromEvent } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  tap,
  filter,
} from "rxjs/operators";
import { SearchService } from "src/app/services/toggler.service";
import { FirebaseService } from "src/app/services/firebase.service";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"],
})
export class SearchComponent implements OnInit, AfterViewInit {
  @ViewChild("input", { static: false }) input: ElementRef;
  @HostBinding("class.search") binded = true;
  @HostBinding("class.is-open") searchIsOpen = false;
  @Output() query = new EventEmitter();

  noResults = false;
  rawItems: any = [];
  items: any = [];
  hasSearched: boolean = false;

  searchForm: FormGroup = this.fb.group({
    queryField: new FormControl(""),
    filter: ["name"],
  });

  constructor(
    public fb: FormBuilder,
    public toggler: SearchService,
    public firebaseService: FirebaseService
  ) {}

  ngOnInit() {
    this.getData();
  }

  ngAfterViewInit() {
    this.toggler.getState().subscribe((state) => (this.searchIsOpen = state));

    fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        filter(Boolean),
        debounceTime(200),
        distinctUntilChanged(),
        tap((event: KeyboardEvent) => {
          const query: any = this.input.nativeElement.value;
          const channel = this.searchForm.get("filter").value;
          // this.query.emit([channel, query]);
          this.search([channel, query]);
        })
      )
      .subscribe();
  }

  getData() {
    this.firebaseService
      .getList()
      .snapshotChanges()
      .subscribe((result) => {
        this.rawItems = result.reverse().map((c: any) => ({
          key: c.payload.key,
          ...c.payload.val(),
        }));
        this.items = [...this.rawItems];
      });
  }

  search(search: string[]) {
    this.hasSearched = true;
    let [channel, query]: string[] = [...search];

    console.log(this.items, channel, query);

    this.items = this.rawItems.filter((item) => {
      if (channel === "id") {
        let id = item.id;
        id = id.toString();
        return id.includes(query);
      }
      query = query.toLocaleLowerCase();
      const originalValue = item[channel].toLocaleLowerCase();
      return originalValue.includes(query);
    });

    if (!this.items.length) {
      this.noResults = true;
    } else {
      this.noResults = false;
    }
  }
}
