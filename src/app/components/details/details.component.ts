import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FirebaseService } from "src/app/services/firebase.service";
import { Observable } from "rxjs";
import { User } from "firebase";
import { ItemInterface } from "src/app/models/item-interface";
import { Title, Meta } from "@angular/platform-browser";

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"],
})
export class DetailsComponent implements OnInit {
  user: Observable<User | null>;
  item: ItemInterface;
  id: string;
  list: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public firebaseService: FirebaseService,
    private title: Title,
    private meta: Meta
  ) {}

  ngOnInit() {
    if (this.item === undefined) {
      this.getSelectedItem();
    } else {
      this.item = this.firebaseService.getSelectedItem();
      this.setSEOTasks(this.item.name, this.item.metaDescription);
    }
  }

  getSelectedItem() {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get("id");
    });

    this.firebaseService
      .getList()
      .snapshotChanges()
      .subscribe((result) => {
        this.list = result.map((c: any) => ({
          key: c.payload.key,
          ...c.payload.val(),
        }));
        this.item = this.list.filter((item) => item.id === Number(this.id));
        this.item = this.item[0];
        this.setSEOTasks(this.item.name, this.item.metaDescription);
      });
  }

  onDelete(key: string) {
    if (confirm(`¿Estás seguro que quieres eliminar a ${this.item.name}?`)) {
      this.firebaseService.delete(key);
      this.router.navigate(["/listado"]);
    }
  }

  setSEOTasks(band: string, metaDescription: string) {
    this.title.setTitle(band);
    this.meta.addTag({
      name: "description",
      content: metaDescription,
    });
  }

  getRandomImage() {
    const randomId = Math.ceil(Math.random() * 300);
    return `https://picsum.photos/id/${randomId}/640/400`;
  }
}
