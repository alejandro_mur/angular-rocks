import { Component, OnInit, HostBinding } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { FirebaseService } from "src/app/services/firebase.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"],
})
export class AddComponent implements OnInit {
  @HostBinding("class.add") binded = true;

  form: FormGroup;

  constructor(
    public fb: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.form = new FormGroup({
      key: new FormControl(null),
      bandName: new FormControl("", Validators.required),
      metaDescription: new FormControl(""),
      info: new FormControl("", Validators.required),
    });
  }

  resetForm(): void {
    this.form.reset();
    this.initForm();
  }

  onSubmit(event: any) {
    this.firebaseService.create(this.form.value).then((res) => {
      this.resetForm();
      this.router.navigate(["/listado"]);
    });
  }
}
