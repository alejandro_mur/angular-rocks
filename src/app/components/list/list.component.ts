import { Component, OnInit } from "@angular/core";
import { FirebaseService } from "src/app/services/firebase.service";
import { Title, Meta } from "@angular/platform-browser";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
})
export class ListComponent implements OnInit {
  // noResults = false;
  rawItems: any = [];
  items: any = [];

  constructor(
    public firebaseService: FirebaseService,
    private title: Title,
    private meta: Meta
  ) {
    this.title.setTitle("Listado de bandas guapas");
    this.meta.removeTag("name='description'");
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.firebaseService
      .getList()
      .snapshotChanges()
      .subscribe((result) => {
        this.rawItems = result.reverse().map((c: any) => ({
          key: c.payload.key,
          ...c.payload.val(),
        }));
        this.items = [...this.rawItems];
      });
  }

  // onSearch(search: string[]) {
  //   let [channel, query]: string[] = [...search];

  //   console.log(this.items, channel, query);

  //   this.items = this.rawItems.filter((item) => {
  //     if (channel === "id") {
  //       let id = item.id;
  //       id = id.toString();
  //       return id.includes(query);
  //     }
  //     query = query.toLocaleLowerCase();
  //     const originalValue = item[channel].toLocaleLowerCase();
  //     return originalValue.includes(query);
  //   });

  //   if (!this.items.length) {
  //     this.noResults = true;
  //   } else {
  //     this.noResults = false;
  //   }
  // }
}
