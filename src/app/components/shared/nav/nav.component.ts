import { Component, OnInit } from "@angular/core";
import { MenuInterface } from "src/app/models/menu-interface";
import { Observable } from "rxjs";
import { SearchService } from "src/app/services/toggler.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"],
})
export class NavComponent implements OnInit {
  menu: MenuInterface[] = [
    {
      text: "Listado",
      link: "/listado",
    },
    {
      text: "Buscar",
      link: "/buscar",
    },
    {
      text: "Añadir",
      link: "/nuevo",
    },
  ];

  constructor(private router: Router, public toggler: SearchService) {}

  ngOnInit() {}
}
