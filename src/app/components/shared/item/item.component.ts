import { Component, OnInit, Input } from "@angular/core";
import { FirebaseService } from "src/app/services/firebase.service";

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"],
})
export class ItemComponent implements OnInit {
  @Input() item: any;

  constructor(public firebaseService: FirebaseService) {}

  ngOnInit() {}

  onGetDetails(item: any) {
    this.firebaseService.selectedItem = Object.assign({}, item);
  }
}
