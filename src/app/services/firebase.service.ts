import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireDatabase } from "@angular/fire/database";
import { ItemInterface } from "../models/item-interface";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FirebaseService {
  itemsList;
  selectedItem: ItemInterface = new ItemInterface();

  constructor(
    private firebase: AngularFireDatabase,
    public db: AngularFirestore
  ) {}

  create(value: any) {
    const timestamp = new Date().getTime();
    const randomID = Math.ceil(Math.random() * 300);

    return this.firebase.list(environment.endpoint).push({
      id: timestamp,
      name: value.bandName,
      image: `https://picsum.photos/id/${randomID}/640/400`,
      metaDescription: value.metaDescription,
      info: value.info,
    } as ItemInterface);
  }

  getSelectedItem() {
    if (Object.keys(this.selectedItem).length !== 0) {
      return this.selectedItem;
    }
    return undefined;
  }

  getList() {
    if (!this.itemsList) {
      this.itemsList = this.firebase.list(environment.endpoint, (ref) =>
        ref.orderByChild("id")
      );
    }
    return this.itemsList;
  }

  updateList() {
    this.itemsList = this.firebase.list(environment.endpoint, (ref) =>
      ref.orderByChild("id")
    );
  }

  getItem(key: string) {
    return this.firebase.object(`${environment.endpoint}/${key}`);
  }

  edit(item: ItemInterface) {
    return this.firebase.list(environment.endpoint).update(item.key, item);
  }

  update(key: string, favourite: boolean) {
    this.firebase.list(environment.endpoint).update(key, { favourite });
    this.updateList();
  }

  delete(key: string) {
    this.firebase.object(`${environment.endpoint}/${key}`).remove();
    this.updateList();
  }
}
