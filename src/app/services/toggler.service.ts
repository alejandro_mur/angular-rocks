import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SearchService {
  private isOpen = new BehaviorSubject(false);

  constructor() {}

  getState(): Observable<boolean> {
    return this.isOpen.asObservable();
  }

  open(): void {
    this.isOpen.next(true);
  }

  close(): void {
    this.isOpen.next(false);
  }
}
