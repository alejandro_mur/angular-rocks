import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { environment } from "../environments/environment";

// Firebase
import { AngularFireModule } from "@angular/fire";

import { AppComponent } from "./app.component";
import { MainComponent } from "./components/shared/main/main.component";
import { NavComponent } from "./components/shared/nav/nav.component";
import { ListComponent } from "./components/list/list.component";
import { ItemComponent } from "./components/shared/item/item.component";
import { AngularFireDatabase } from "@angular/fire/database";
import { AngularFirestore } from "@angular/fire/firestore";
import { DetailsComponent } from "./components/details/details.component";
import { SearchComponent } from "./components/search/search.component";
import { AddComponent } from './components/add/add.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    ListComponent,
    ItemComponent,
    DetailsComponent,
    SearchComponent,
    AddComponent,
  ],
  providers: [AngularFireDatabase, AngularFirestore],
  bootstrap: [AppComponent],
})
export class AppModule {}
