// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endpoint: "bands",
  firebase: {
    apiKey: "AIzaSyAO5VnQiF6zOuChHSMwHCfBRyCUlkl9O9U",
    authDomain: "angular-rocks-54418.firebaseapp.com",
    databaseURL: "https://angular-rocks-54418.firebaseio.com",
    projectId: "angular-rocks-54418",
    storageBucket: "angular-rocks-54418.appspot.com",
    messagingSenderId: "96876962044",
    appId: "1:96876962044:web:a720b1ab62064a86463779",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
